# SeanDowney\BackpackStoreCrud

[![Latest Version on Packagist][ico-version]](link-packagist)
[![Software License][ico-license]](LICENSE.md)
[![Total Downloads][ico-downloads]][link-downloads]

An admin interface to easily add/edit/remove Store Products and Categories, using [Laravel Backpack](laravelbackpack.com).

## Install

1) In your terminal:

``` bash
$ composer require seandowney/backpackstorecrud
```

2) Add the service provider to your config/app.php file:
```php
SeanDowney\BackpackStoreCrud\StoreCrudServiceProvider::class,
```

3) Publish the config file & run the migrations
```bash
$ php artisan vendor:publish --provider="SeanDowney\BackpackStoreCrud\StoreCrudServiceProvider" #publish config, view  and migration files
$ php artisan migrate #create the store tables
```

4) [Optional] Add a menu item for it in resources/views/vendor/backpack/base/inc/sidebar.blade.php or menu.blade.php:

```html
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> <span>Store</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/product') }}"><i class="nav-icon fa fa-newspaper-o"></i> <span>Products</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/category') }}"><i class="nav-icon fa fa-newspaper-o"></i> <span>Categories</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/price_option') }}"><i class="nav-icon fa fa-list"></i> <span>Price Options</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/price_group') }}"><i class="nav-icon fa fa-tag"></i> <span>Price Groups</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/delivery_option') }}"><i class="nav-icon fa fa-list"></i> <span>Delivery Options</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/delivery_group') }}"><i class="nav-icon fa fa-tag"></i> <span>Delivery Groups</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url(config('seandowney.storecrud.route_prefix', 'store').'/order') }}"><i class="nav-icon fa fa-tag"></i> <span>Orders</span></a></li>
  </ul>
</li>
```

5) [Optional] Add a basic Store frontend using the base controllers.

* Add the Stripe settings to the `config/services.php` file in your project
    ```
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'endpoint_secret' => env('STRIPE_ENDPOINT_SECRET'),
    ],
    ```
* Add the Stripe public key to you page `<meta name="stripe-key" content="{{ config('services.stripe.key') }}">`
* If you want to use the Vue components, add the `store.js` to your `resources/js/app.js` file
* Add Vue to your `package.json` file - `"vue": "^2.6.12"`
* Add the following routes to your `routes/web.php` file:

```php
/**
 * Store Routes
 */
Route::group(['prefix' => 'api/'.config('seandowney.storecrud.route_prefix', 'store')], function () {
    Route::post('country', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\ApiController@countryDelivery']);

    Route::get('cart', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\ApiController@cartSummary']);
    Route::delete('cart', '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\ApiController@clearCart');

    Route::post('cart/items', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\ApiController@addItem']);
    Route::delete('cart/items/{skuCode}', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\ApiController@removeItem']);
});

Route::group(['prefix' => config('seandowney.storecrud.route_prefix', 'store')], function () {
    // handle the cart
    Route::get('cart', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\CartController@index']);
    Route::get('address', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\CartController@address']);
    Route::post('address', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\CartController@storeAddress']);
    Route::get('checkout', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\CartController@checkout']);
    Route::post('pay', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\CartController@pay']);
    Route::get('thankyou', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\CartController@thankyou']);

    Route::get('/', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\StoreController@index']);
    Route::get('{category}/{product}/{subs?}', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\StoreController@product'])
        ->where(['category' => '^((?!admin).)*$', 'product' => '^((?!admin).)*$', 'subs' => '.*']);
    Route::get('{category}', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\StoreController@category'])
        ->where(['category' => '^((?!admin).)*$']);
});
```

* Add the following route to your `routes\api.php` file:

```php
/**
 * Store Route for Stripe webhooks
 */
Route::group(['prefix' => config('seandowney.storecrud.route_prefix', 'store')], function () {
    Route::post('complete', ['uses' => '\SeanDowney\BackpackStoreCrud\app\Http\Controllers\ApiController@completePurchase']);
});
```

* Add the webhook to your Stripe account for the `payment_intent.succeeded` event

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.


## Testing

``` bash
// TODO
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email sean@considerweb.com instead of using the issue tracker.

## Credits

- Seán Downey - Lead Developer
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/seandowney/backpackstorecrud.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/seandowney/backpackstorecrud.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/seandowney/backpackstorecrud
[link-downloads]: https://packagist.org/packages/seandowney/backpackstorecrud
[link-contributors]: ../../contributors
