import Vue from 'vue';
import ReviewCart from './components/ReviewCart.vue';
import HeaderCart from './components/HeaderCart.vue';
import Product from './components/Product.vue';

const app = new Vue({
    el: '#app',
    components: {
        ReviewCart,
        HeaderCart,
        Product
    }
});
