@extends('layouts.default')

{{-- Page title --}}
@section('title')
Shop Payment ::
@parent
@stop

@section('footer')
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
  var stripe = Stripe('{{ config('services.stripe.key') }}');
    // Create an instance of elements
    var elements = stripe.elements();

    var style = {
        base: {
            fontWeight: 400,
            fontFamily: '"DM Sans", Roboto, Open Sans, Segoe UI, sans-serif',
            fontSize: '16px',
            lineHeight: '1.4',
            color: '#1b1642',
            padding: '.75rem 1.25rem',
            '::placeholder': {
                color: '#ccc',
            },
        },
        invalid: {
            color: '#dc3545',
        }
    };

    var cardElement = elements.create('cardNumber', {
        style: style
    });
    cardElement.mount('#card_number');

    var exp = elements.create('cardExpiry', {
        'style': style
    });
    exp.mount('#card_expiry');

    var cvc = elements.create('cardCvc', {
        'style': style
    });
    cvc.mount('#card_cvc');

    var postalCode = elements.create('postalCode', {
        'style': style
    });
    postalCode.mount('#postalCode');

    // Validate input of the card elements
    var resultContainer = document.getElementById('paymentResponse');
    cardElement.addEventListener('change', function (event) {
        if (event.error) {
            resultContainer.innerHTML = '<p>' + event.error.message + '</p>';
        } else {
            resultContainer.innerHTML = '';
        }
    });

    // Get payment form element
    var form = document.getElementById('payment-form');
    var formButton=document.getElementById('pay-but');
    let displayError = document.getElementById('card-errors');

    // Create a token when the form is submitted.
    form.addEventListener('submit', function (e) {
      e.preventDefault();
      displayError.textContent = '';
      formButton.disabled = true;

        stripe.confirmCardPayment(formButton.dataset.secret, {
          payment_method: {
            card: cardElement
          }
        }).then(function(result) {
          if (result.error) {
            // Show error to your customer (e.g., insufficient funds)
            displayError.textContent = result.error.message;
            formButton.disabled = false;
          } else {
            // The payment has been processed!
            if (result.paymentIntent.status === 'succeeded') {
              // Show a success message to your customer
              // There's a risk of the customer closing the window before callback
              // execution. Set up a webhook or plugin to listen for the
              // payment_intent.succeeded event that handles any business critical
              // post-payment actions.
              stripeTokenHandler(result.paymentIntent);
            }
          }
        });
    });

    // Callback to handle the response from stripe
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        // Submit the form
        form.submit();
    }

    document.querySelector('.pay-via-stripe-btn').addEventListener('click', function () {
        // var payButton   = $(this);

        var ts_cs_error = document.getElementById('error-terms');
        if(!document.getElementById('terms_conditions').checked){
            ts_cs_error.innerHTML = 'The terms conditions must be accepted.';
            return false;
        } else {
            ts_cs_error.innerHTML = '';
        }
    });
</script>
@endsection

{{-- Page content --}}
@section('content')
<div class="page-title">
  <div class="overlay"></div>
  <h1>Shop</h1>
</div>

<div class="light-wrapper">
      <div class="container">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Checkout</h1>
    </div>

</div>
<!-- /.row -->

<!-- Portfolio Item Row -->
<div class="row">
  <div class="col-sm-8 content">
    <div class="form-container">
    <form action="{{ url('/'.config('seandowney.storecrud.route_prefix', 'store').'/pay') }}" method="POST" id="payment-form">
        {{ csrf_field() }}
      <div class="form-row">
        <h2>Final Costs</h2>
        <p>Sub Total: {{ $currency }}<span id="subtotal">{{ $subTotal }}</span></p>
        <p>Delivery: {{ $currency }}<span id="deliveryCost">{{ $standardDelivery }}</span></p>
        <p>Total: {{ $currency }}<span id="totalCost">{{ $total }}</span></p>
      </div>

      <div class="form-row">
        <h2>Card Details</h2>
        <p><strong>Note:</strong> All the payments are handled by <a target="_blank" href="https://stripe.com">Stripe</a>. We don't store your card details.</p>
      </div>

      <div id="paymentForm">
        <div id="card-errors" class="text-danger payment-errors" role="alert"></div>

        <div class="row form-group">
            <div class="col-md-12">
                <!-- Display errors returned by createToken -->
                <label>Card Number</label>
                <div id="paymentResponse" class="text-danger font-italic"></div>
                <div id="card_number" class="field form-control"></div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-3">
                <label>Expiry Date</label>
                <div id="card_expiry" class="field form-control"></div>
            </div>
            <div class="col-md-3">
                <label>CVC Code</label>
                <div id="card_cvc" class="field form-control"></div>
            </div>
            <div class="col-md-3">
                <label>Post Code</label>
                <div id="postalCode" class="field form-control"></div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-check form-check-inline custom-control custom-checkbox">
                <label for="terms_conditions" class="custom-control-label">
                    <input type="checkbox" name="terms_conditions" id="terms_conditions" class="custom-control-input">
                    I agree to terms & conditions
                  </label>
            </div>
            {{-- @if($errors->first('terms_conditions')) --}}
            <div class="text-danger font-italic" id="error-terms">{{ $errors->first('terms_conditions') }}</div>
            {{-- @endif --}}
        </div>

        <button type="submit" id="pay-but" class="btn pay-via-stripe-btn" data-secret="{{ $intent_client_secret }}">Submit Payment</button>
      </div>
    </form>
    </div>
  </div>
  <aside class="col-sm-4 sidebar lp30">
  </aside>
</div>
      </div>
</div>
@endsection
