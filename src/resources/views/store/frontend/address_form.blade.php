@extends('layouts.default')

{{-- Page title --}}
@section('title')
{{{ $title }}} ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="page-title">
  <div class="overlay"></div>
  <h1>Shop</h1>
</div>

<div class="light-wrapper">
      <div class="container">

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Checkout</h1>
    </div>

</div>
<!-- /.row -->

<!-- Portfolio Item Row -->
<div class="row">
  <div class="col-sm-8 content">
    <div class="form-container">
    <form action="{{ url('/'.config('seandowney.storecrud.route_prefix', 'store').'/address') }}" method="POST">
        {{ csrf_field() }}

      <div class="field">
        <label class="label">Your Name</label>
        <div class="control">
          <input class="input" type="text" name="name" id="name" size="100" value="{{ old('name') }}">
          <p class="help is-danger">{{ $errors->first('name') }}</p>
        </div>
      </div>

      <div class="field">
        <label class="label">Your Email</label>
        <div class="control has-icons-left">
            <input class="input" type="email" name="email" id="email" size="100" value="{{ old('email') }}" placeholder="hello@myemailaddress.com">
            <span class="icon is-small is-left">
                <i class="fas fa-envelope"></i>
            </span>
            <p class="help is-danger">{{ $errors->first('email') }}</p>
        </div>
      </div>

      <div class="field">
        <label class="label">Your Phone</label>
        <div class="control has-icons-left">
            <input class="input" type="tel" id="phone" name="phone" pattern="(00|\+)[0-9]{9,15}" size="100" value="{{ old('phone') }}" placeholder="eg 00353871111111">
            <p class="help is-danger">{{ $errors->first('phone') }}</p>
        </div>
      </div>

      <div class="field">
        <br>
      </div>
      <div class="field">
        <h2>Delivery Address</h2>
      </div>

      <div class="field">
        <label class="label">Address Line 1</label>
        <div class="control">
            <input class="input" type="text" name="address1" size="200" value="{{ old('address1') }}">
            <p class="help is-danger">{{ $errors->first('address1') }}</p>
        </div>
      </div>

      <div class="field">
        <label class="label">Address Line 2</label>
        <div class="control">
            <input class="input" type="text" name="address2" size="200" value="{{ old('address2') }}">
            <p class="help is-danger">{{ $errors->first('address2') }}</p>
        </div>
      </div>

      <div class="field">
        <label class="label">Town/City</label>
        <div class="control">
            <input class="input" type="text" name="address_city" size="200" value="{{ old('address_city') }}">
            <p class="help is-danger">{{ $errors->first('address_city') }}</p>
        </div>
      </div>

      <div class="field">
        <label class="label">County/State</label>
          <div class="control">
            <input class="input" type="text" name="address_state" size="200" value="{{ old('address_state') }}">
            <p class="help is-danger">{{ $errors->first('address_state') }}</p>
          </div>
      </div>

      <div class="field">
        <label class="label">Country</label>
        <div class="control">
            <div class="select">
            <select name="country" id="country">
            @foreach($countries as $code => $name)
            <option value="{{ $code }}" {{ (old('country') == $code ? "selected":"") }}>{{ $name }}</option>
            @endforeach
            </select>
            </div>
            <p class="help is-danger">{{ $errors->first('country') }}</p>
        </div>
      </div>

      <div class="field">
        <label class="label">Postal Code</label>
        <div class="control">
            <input class="input" type="text" name="postcode" size="6" value="{{ old('postcode') }}">
            <p class="help is-danger">{{ $errors->first('postcode') }}</p>
        </div>
      </div>

      <div class="field">
        <div class="control">
            <button type="submit" id="pay-but" class="button is-primary pay-via-stripe-btn">Continue</button>
        </div>
      </div>
    </form>
    </div>
  </div>
  <aside class="col-sm-4 sidebar lp30">
  </aside>
</div>
      </div>
</div>
@endsection
