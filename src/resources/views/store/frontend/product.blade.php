@extends('layouts.default')

{{-- Page title --}}
@section('title')
{{{ $title }}} ::
@parent
@stop


{{-- Page content --}}
@section('content')
<div class="page-title">
    <div class="overlay"></div>
    <h1>Shop</h1>
</div>
<!-- Page Heading/Breadcrumbs -->
<div class="light-wrapper">
    <div class="container">


          <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li><a href="{{ url('/'.config('seandowney.storecrud.route_prefix', 'store')) }}">Shop</a></li>
              <li><a href="{{ url('/'.config('seandowney.storecrud.route_prefix', 'store').'/'.$category->slug) }}">{{ $category->title }}</a></li>
              <li class="is-active"><a href="#" aria-current="page">{{ $product->title }}</a></li>
            </ul>
          </nav>
          <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $product->title }}</h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">
            @if($images)
            <div class="col-md-6">
                @include('seandowney::frontend.partials.carousel', ['images' => $images])
            </div>
            @endif

            <div class="col-md-6">
                @if($product->materials)<p><b>{{ $product->materials }}</b></p>@endif
                @if($product->size)<p><b>Size:</b> {{ $product->size }}</p>@endif
                @if($options)
                <product
                    shopuri="{{ config('seandowney.storecrud.route_prefix', 'store') }}"
                    @if($product->remaining_num) v-bind:remainingnum="{{ $product->remaining_num }}" @endif
                    @if($product->total_num) v-bind:totalnum="{{ (int)$product->total_num }}" @endif
                    productcode="{{ $product->code }}"
                    currency="{{ config('seandowney.storecrud.currency.symbol', '€') }}"
                    v-bind:options="{{ json_encode($options) }}"></product>
                @endif

            </div>

        </div>
        <div class="row">

            <div class="col-lg-12">
                <h3>Description</h3>
                {!! $product->description !!}
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
@stop
