@extends('layouts.default')

{{-- Page title --}}
@section('title')
{{{ $title }}} ::
@parent
@stop


{{-- Page content --}}
@section('content')
<div class="page-title">
    <div class="overlay"></div>
    <h1>Shop</h1>
  </div>


<div class="light-wrapper">
    <div class="container">


          <!-- Page Heading/Breadcrumbs -->
        <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
            <li><a href="{{ url('/'.config('seandowney.storecrud.route_prefix', 'store')) }}">Shop</a></li>
            <li class="is-active"><a href="#" aria-current="page">{{ $category->title }}</a></li>
        </ul>
        </nav>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $category->title }}</h1>
                {!! $category->description !!}
            </div>
        </div>
        <!-- /.row -->

        <div class="row product-list">
        @foreach ($products as $product)
            @include('seandowney::frontend.partials.product_item', ['product' => $product])
        @endforeach
        </div>
    </div>
</div>
@stop
