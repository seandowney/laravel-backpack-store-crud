<?php

Route::group([
    'namespace' => 'SeanDowney\BackpackStoreCrud\app\Http\Controllers\Admin',
    'prefix' => config('backpack.base.route_prefix', 'admin').'/'.config('seandowney.storecrud.route_prefix', 'store'),
    'middleware' => ['web', backpack_middleware()]
], function () {
    Route::get('/', function () {
        return redirect('category');
    });
    Route::crud('category', 'CategoryCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('price_option', 'PriceOptionCrudController');
    Route::crud('price_group', 'PriceGroupCrudController');
    Route::crud('delivery_option', 'DeliveryOptionCrudController');
    Route::crud('delivery_group', 'DeliveryGroupCrudController');
    Route::crud('order', 'OrderCrudController');
});
