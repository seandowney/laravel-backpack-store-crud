<?php

namespace SeanDowney\BackpackStoreCrud\app\Http\Controllers;

use App\Http\Controllers\Controller;
use SeanDowney\BackpackStoreCrud\app\Cart\Cart;
use SeanDowney\BackpackStoreCrud\app\Models\Order;
use Stripe\PaymentIntent;

class CartController extends Controller
{
    protected $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index()
    {
        $viewData['title'] = 'Basket';

        /**
         * @TODO Check that the quantity selected is still available
         */

        return view('seandowney::frontend.cart', $viewData);
    }


    /**
     * Show the address form
     *
     * @return void
     */
    public function address()
    {
        $viewData['title'] = '';
        $viewData['countries'] = config('seandowney.storecrud.countries');
        $viewData['country'] = old('country') ? old('country') : $this->cart->getDeliveryCountry();

        return view('seandowney::frontend.address_form', $viewData);
    }


    /**
     * Store the address and create an Order in the system
     * It will be a Payment Pending order until the person pays in the checkout
     *
     * @return void
     */
    public function storeAddress()
    {
        request()->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'phone' => [
                'required',
                'regex:/(00|\+)[0-9]{9,15}/',
            ],
            'address1' => 'required|min:5',
            'address_city' => 'required|min:3',
            'address_state' => 'required|min:2',
            'country' => 'required|min:2|max:2',
        ]);

        $order_id = session('order_id');
        if (!empty($order_id)) {
            // delete the old order record
            $order = Order::find($order_id);

            // Cancel the payment intent for this order
            $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));
            $stripe->paymentIntents->cancel($order->transaction_id, []);

            $order->delete();

            $order_id = '';
        }

        if (empty($order_id)) {
            /**
             * @TODO Check that the quantity selected is still available
             */

            $country = request()->get('country');

            $deliveryPrice = $this->cart->deliveryCostForCountry($country);
            $deliveryPrice = is_null($deliveryPrice) ? 0 : $deliveryPrice;

            $orderNum = Order::generateToken();
            $data = request()->only([
                'name', 'email', 'phone', 'address1', 'address2', 'address_city', 'address_state', 'country', 'postcode',
            ]);
            $data['orderNum'] = $orderNum;
            $data['delivery'] = $deliveryPrice;

            $order = $this->cart->convertToOrder($data);

            // set the order id
            session()->put('order_id', $order->id);

            return redirect('/'.config('seandowney.storecrud.route_prefix', 'store').'/checkout');
        }

        return redirect('/'.config('seandowney.storecrud.route_prefix', 'store').'/cart');
    }


    /**
     * Display the final costs and handle the payment with Stripe
     * Create a Payment Intent and then show the form to allow the person to pay
     *
     * @return void
     */
    public function checkout()
    {
        $orderSummary = $this->cart->summary();
        $order_id = session('order_id');

        $order = Order::find($order_id);

        /**
         * @TODO Check that the quantity selected is still available
         */

        $intent = PaymentIntent::create([
            'amount' => $orderSummary['total'] * 100,
            'currency' => config('seandowney.storecrud.currency.code', 'EUR'),
            // Verify your integration in this guide by including this parameter
            'metadata' => [
                'customer_name'  => $order->name,
                'customer_email' => $order->email,
                'order_num'      => $order->order_num,
                'order_id'       => $order_id,
            ],
            'receipt_email' => $order->email,
        ]);

        // Update the order with the new PaymentIntent id
        $order->transaction_id = $intent->id;
        $order->save();

        $viewData['title'] = '';
        $viewData['countries'] = config('seandowney.storecrud.countries');
        $viewData['country'] = old('country') ? old('country') : $this->cart->getDeliveryCountry();

        $viewData['currency'] = config('seandowney.storecrud.currency.symbol');
        $viewData['standardDelivery'] = $orderSummary['shipping'];
        $viewData['subTotal'] = $orderSummary['sub_total'];
        $viewData['total'] = $orderSummary['total'];
        $viewData['intent_client_secret'] = $intent->client_secret;

        return view('seandowney::frontend.purchase_form', $viewData);
    }


    /**
     * Process the purchase
     */
    public function pay()
    {
         request()->validate([
            'terms_conditions' => 'accepted',
            'stripeToken' => 'required',
        ]);

        if (empty(request()->get('stripeToken'))) {
            session()->flash('error', 'Some error while making the payment. Please try again');
            return back()->withInput();
        }

        $order = Order::find(session('order_id'));

        // clear the cart
        $this->cart->clear();

        $viewData = [];
        $viewData['order_id'] = $order->id;
        $viewData['order_num'] = $order->order_num;
        $viewData['amount'] = $order->total;
        return redirect('/'.config('seandowney.storecrud.route_prefix', 'store').'/thankyou')->with('order_id', $order->id);
    }

    public function thankyou()
    {
        $order = Order::find(session('order_id'));
        return view('seandowney::frontend.thankyou', ['order' => $order]);
    }
}
