<?php

namespace SeanDowney\BackpackStoreCrud\app\Http\Controllers;

use SeanDowney\BackpackStoreCrud\app\Models\Category;
use App\Http\Controllers\Controller;
use SeanDowney\BackpackStoreCrud\app\Models\Product;

class StoreController extends Controller
{
    public function index()
    {
        $categories = Category::published()->orderBy('lft')->get();

        if (!$categories) {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $data['title'] = 'The Store';
        $data['categories'] = $categories;

        return view('seandowney::frontend.index', $data);
    }

    public function category($slug)
    {
        $category = Category::whereSlug($slug)->published()->first();

        if (!$category) {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $data['title'] = $category->title;
        $data['category'] = $category->withFakes();
        $data['products'] = $category->products()->published()->orderBy('order')->get();

        return view('seandowney::frontend.category', $data);
    }


    public function product($category, $slug)
    {
        $category = Category::whereSlug($category)->published()->first();
        $product = Product::whereSlug($slug)->published()->first();

        if (!$product) {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $data['title'] = $product->title;
        $data['category'] = $category->withFakes();
        $data['product'] = $product->withFakes();
        $data['images'] = $product->images;
        $options = $product->options();

        $data['options'] = $options->map(function ($item, $key) {
            return [
                'id' => $item->id,
                'title' => $item->title,
                'label' => $item->label,
                'code' => $item->code,
                'price' => $item->price,
            ];
        });

        return view('seandowney::frontend.product', $data);
    }
}
