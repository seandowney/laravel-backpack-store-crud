<?php

namespace SeanDowney\BackpackStoreCrud\app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SeanDowney\BackpackStoreCrud\app\Cart\Cart;
use SeanDowney\BackpackStoreCrud\app\Events\OrderReceived;
use SeanDowney\BackpackStoreCrud\app\Models\Order;
use Stripe\Event;
use Stripe\Webhook;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Exception\UnexpectedValueException;

class ApiController extends Controller
{
    protected $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function cartSummary()
    {
        return $this->cart->summary();
    }


    public function clearCart()
    {
        $this->cart->clear();

        return $this->cart->summary();
    }


    public function addItem(Request $request)
    {
        request()->validate([
            'sku' => 'required',
            'quantity' => 'required|integer',
        ]);

        $sku = request()->get('sku');

        $item = $this->cart->addItem($sku, request()->get('quantity'));

        return $this->cart->summary();
    }


    public function removeItem(Request $request, $skuCode)
    {
        $this->cart->removeItem($skuCode);

        return $this->cart->summary();
    }



    /**
     * Calculate the delivery price for the country
     *
     * @param Request $request
     * @return void
     */
    public function countryDelivery(Request $request)
    {
        request()->validate([
            'country' => 'required|max:2',
        ]);

        $country = request()->get('country');

        $deliveryDetails = [
            'message' => 'Sorry no delivery available to this country: '.$country,
        ];

        $deliveryPrice = $this->cart->deliveryCostForCountry($country);

        if (!is_null($deliveryPrice)) {
            $this->cart->setDeliveryCountry($country);
            return $this->cart->summary();
        }

        // check if there is a specific delivery option for this
        return response()->json($deliveryDetails);
    }


    /**
     * This is the Stripe webhook endpoint
     *
     * @param Request $request
     * @return void
     */
    public function completePurchase(Request $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $event = null;
        logger('ApiController complete');

        if (!app()->environment('local')) {
            // The environment is not local
            try {
                $event = Webhook::constructEvent(
                    $request->all(), $request->header('HTTP_STRIPE_SIGNATURE'), config('services.stripe.endpoint_secret')
                );
            } catch(UnexpectedValueException $e) {
                // Invalid payload
                logger()->error('UnexpectedValueException: '.$e->getMessage());
                http_response_code(400);
                exit();
            } catch(SignatureVerificationException $e) {
                // Invalid signature
                logger()->error('SignatureVerificationException: '.$e->getMessage());
                http_response_code(400);
                exit();
            }
        } else {
            // use this method for testing on the local environment using the Stripe CLI
            try {
                $event = Event::constructFrom(
                    $request->all()
                );
            } catch(\UnexpectedValueException $e) {
                // Invalid payload
                http_response_code(400);
                exit();
            }
        }

        // Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object; // contains a StripePaymentIntent
                $this->handlePaymentIntentSucceeded($paymentIntent);
                break;
            default:
                echo 'Received unknown event type ' . $event->type;
        }

        // check if there is a specific delivery option for this
        return response('', 200);
    }



    /**
     * Handle the Stripe Webhook event that the Payment Intent has Succeeded
     * The payment was received
     *
     * @param \Stripe\PaymentIntent $paymentIntent
     * @return void
     */
    private function handlePaymentIntentSucceeded($paymentIntent)
    {
        if (!empty($paymentIntent->metadata->order_num)) {
            $order_id = $paymentIntent->metadata->order_id;
            $order_num = $paymentIntent->metadata->order_num;

            $order = Order::where('id', $order_id)
                ->where('order_num', $order_num)
                ->first();

            if (!$order) {
                logger('ApiController handlePaymentIntentSucceeded: no Order matching order_num='.$order_num);
                exit;
            }

            $charge = $paymentIntent->charges->data[0];

            $order->transaction_id = $paymentIntent->id;
            $order->payment_status = $charge['status'];
            $order->receipt_url    = $charge['receipt_url'];
            $order->status         = ORDER_RECEIVED;
            $order->save();

            event(new OrderReceived($order));
        } else {
            logger('ApiController handlePaymentIntentSucceeded: no Order num');
        }
    }


}
