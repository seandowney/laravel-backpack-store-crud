# Changelog

All Notable changes to `Sean Downey Laravel Backpack Store Crud` will be documented in this file

## 2.0.2 - 2020-03-14

### Updated
- Small update to the ReviewCart front end component

## 2.0.1 - 2020-02-20

### Updated
- Updating the Readme file installation instructions for the routes
- Improve how the `to` email address of the NewOrders email is configured

## 2.0.0 - 2020-02-20

This is a breaking change to upgrade to Backpack CRUD 4.1

### Updated
- Updating to Backpack CRUD 4.1
- Updating the basic frontend templates
- Improving the routes and controller setup for the Frontend
- Updating the installation instructions

## NEXT - YYYY-MM-DD

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

## 1.1.0 - 2020-12-20

### Updated
- New 3D payments through Stripe - #8
   - updated purchase flow to handle the PaymentIntents from Stripe
   - New order status - Update your `config/seandowney/storecrud.php` file
   - Stripe transaction numbers will now use the Payment Intent id rather than the Charge id
   - New routes added - see the Readme file

## 0.0.1 - 2016-12-11

### Added
- Initial code.
